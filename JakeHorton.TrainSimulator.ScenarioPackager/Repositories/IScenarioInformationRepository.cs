﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System.Collections.Generic;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Repositories
{
    public interface IScenarioInformationRepository
    {
        List<ScenarioInfo> GetAll();
    }
}
