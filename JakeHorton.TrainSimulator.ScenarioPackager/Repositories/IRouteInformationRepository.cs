﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System.Collections.Generic;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Repositories
{
    public interface IRouteInformationRepository
    {
        public List<RouteInfo> GetAll();
    }
}
