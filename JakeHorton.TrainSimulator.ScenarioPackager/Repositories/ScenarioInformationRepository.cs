﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Repositories
{
    class ScenarioInformationRepository : IScenarioInformationRepository, IDisposable
    {
        private readonly List<DirectoryInfo> _scenarioFolders;
        private bool disposedValue;
        public ScenarioInformationRepository(string routeFolder)
        {
            try
            {
                _scenarioFolders = Directory.GetDirectories(Path.Join(routeFolder, @"Scenarios"))
                                            .Select(folder => new DirectoryInfo(folder))
                                            .ToList();
            }
            catch (IOException exception)
            {
                throw new ArgumentException($"Could not find the scenarios folder in '{routeFolder}'", exception);
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public List<ScenarioInfo> GetAll() =>
            _scenarioFolders.Select(folder => GetScenarioInformation(folder))
                            .Where(scenarioInformation => scenarioInformation != null)
                            .OrderByDescending(scenarioInformation => scenarioInformation.LastModified)
                            .ToList();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }
                disposedValue = true;
            }
        }

        private static ScenarioInfo GetScenarioInformation(DirectoryInfo folder)
        {
            var scenarioProperties = folder.GetFiles()
                                           .FirstOrDefault(file => file.Name == "ScenarioProperties.xml");
            return scenarioProperties != null 
                ? ScenarioInfo.FromXmlFile(scenarioProperties.FullName) 
                : null;
        }
    }
}