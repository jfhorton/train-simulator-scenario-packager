﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Repositories
{
    class RouteInformationRepository : IRouteInformationRepository, IDisposable
    {
        private const string ArchiveName = "MainContent.ap";
        private const string FileName = "RouteProperties.xml";
        private readonly List<DirectoryInfo> _routeFolders;
        private bool disposedValue;
        public RouteInformationRepository(string trainSimulatorInstallationFolder)
        {
            try
            {
                _routeFolders = Directory.GetDirectories(Path.Join(trainSimulatorInstallationFolder, @"Content\Routes"))
                                         .Select(folder => new DirectoryInfo(folder))
                                         .ToList();
            }
            catch(IOException exception)
            {
                throw new ArgumentException($"Could not find the routes folder in '{trainSimulatorInstallationFolder}'", exception);
            }
        }

        public static bool MainContentApExists(DirectoryInfo directoryInfo) =>
            directoryInfo.GetFiles().Any(fileInfo => fileInfo.Name == ArchiveName);

        public static bool RoutePropertiesXmlExists(DirectoryInfo directoryInfo) =>
            directoryInfo.GetFiles().Any(fileInfo => fileInfo.Name == FileName);

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public List<RouteInfo> GetAll() =>
            _routeFolders.Select(folder => GetRouteInformation(folder))
                         .Where(routeInformation => routeInformation != null)
                         .OrderBy(routeInformation => routeInformation.Name)
                         .ToList();

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing) { }

                disposedValue = true;
            }
        }

        private static RouteInfo GetRouteInformation(DirectoryInfo directoryInfo)
        {
            try
            {
                if (RoutePropertiesXmlExists(directoryInfo))
                {
                    return RouteInfo.FromXmlFile(Path.Join(directoryInfo.FullName, FileName));
                }
                else if (MainContentApExists(directoryInfo))
                {
                    return RouteInfo.FromApArchive(Path.Join(directoryInfo.FullName, ArchiveName));
                }
                return null;
            }
            catch(XmlException)
            {
                return null;
            }
        }
    }
}
