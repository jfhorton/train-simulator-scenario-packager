﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Views
{
    public partial class MainWindowView : Form, IMainWindowView
    {
        public MainWindowView()
        {
            InitializeComponent();
            Load += (s, e) => ApplicationLoaded?.Invoke(this, null);
            autoDetectButton.Click += (s, e) => AutoDetectClicked?.Invoke(this, null);
            setTrainSimulatorFolderButton.Click += (s, e) => SetTrainSimulatorFolderClicked?.Invoke(this, null);
            routeListBox.SelectedIndexChanged += (s, e) => routesBindingSource.Position = routeListBox.SelectedIndex;
            routesBindingSource.CurrentChanged += (s, e) => SelectedRouteChanged?.Invoke(this, null);
            scenariosBindingSource.CurrentChanged += (s, e) => SelectedScenarioChanged?.Invoke(this, null);

            criteriaTextBox.TextChanged += (s, e) =>
            {
                scenariosBindingSource.Filter = $"Name LIKE '*{criteriaTextBox.Text}*'";
            };

            criteriaTextBox.KeyPress += (s, e) =>
            {
                if ("*%[]\"'".ToArray().Contains(e.KeyChar))
                {
                    e.Handled = true;
                }            
            };

            scenarioDataGridView.DataBindingComplete += (s, e) =>
            {
                if (scenariosBindingSource.List.Count > 0)
                {
                    scenarioDataGridView.Columns["Folder Path"].Visible = false;
                }
            };

            exitMenuItem.Click += (s, e) => Environment.Exit(0);
            aboutMenuItem.Click += (s, e) => MessageBox.Show
            (
                caption: "Train Simulator Scenario Packager",
                text: "Version 1.0.1\n\nWritten by Jake Horton\n\nAdditional credits: Robert Cronshaw, Ben Penhalagan, Darren Wardlaw, Jake Colclough, Taylor Haggarty",
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information
            );

            packageButton.Click += (s, e) => PackageScenarioClicked?.Invoke(this, null);
        }

        public event EventHandler ApplicationLoaded;

        public event EventHandler AutoDetectClicked;

        public event EventHandler PackageScenarioClicked;

        public event EventHandler SelectedRouteChanged;

        public event EventHandler SelectedScenarioChanged;

        public event EventHandler SetTrainSimulatorFolderClicked;

        public string ScenarioZipFileName => packageScenarioDialog.FileName;

        public string SelectedRoutePath => (string)routeListBox.SelectedValue;

        public string SelectedScenarioPath
        {
            get
            {
                if (scenarioDataGridView.SelectedRows.Count > 0)
                {
                    return (string)scenarioDataGridView.SelectedRows[0].Cells["Folder Path"].Value;
                }
                return null;
            }
        }

        public string StatusText
        {
            get => statusLabel.Text;
            set => statusLabel.Text = value;
        }

        public string TrainSimulatorInstallationFolder
        {
            get => trainSimulatorFolderTextbox.Text;
            set { trainSimulatorFolderTextbox.Text = value; }
        }
        public static DataTable ConvertToDataTable(List<RouteInfo> info)
        {
            var dataTable = new DataTable();
            foreach (var columnName in new List<string> { "Name", "Folder Path" })
            {
                dataTable.Columns.Add(columnName, typeof(string));
            }
            foreach (var route in info)
            {
                dataTable.Rows.Add(route.Name, route.FolderPath);
            }
            return dataTable;
        }

        public static DataTable ConvertToDataTable(List<ScenarioInfo> info)
        {
            var dataTable = new DataTable();
            foreach (var columnName in new List<string> { "Name", "Author", "Folder Path", "Type" })
            {
                dataTable.Columns.Add(columnName, typeof(string));
            }
            dataTable.Columns.Add("Last Modified", typeof(DateTime));

            foreach (var scenario in info)
            {
                dataTable.Rows.Add(scenario.Name, scenario.Author, scenario.FolderPath, scenario.Type, scenario.LastModified);
            }
            return dataTable;
        }

        public void ClearRouteList() => routeListBox.DataSource = null;

        public void ClearScenarioList() => scenarioDataGridView.DataSource = null;

        public void SetRouteList(List<RouteInfo> routes)
        {
            routesBindingSource.DataSource = ConvertToDataTable(routes);
            routeListBox.DataSource = routesBindingSource;
            routeListBox.DisplayMember = "Name";
            SelectedRouteChanged?.Invoke(this, null);
        }
        public void SetScenarioList(List<ScenarioInfo> scenarios)
        { 
            criteriaTextBox.Clear();
            scenariosBindingSource.RemoveFilter();
            scenariosBindingSource.DataSource = ConvertToDataTable(scenarios);
            scenarioDataGridView.DataSource = scenariosBindingSource;
            scenarioDataGridView.AutoResizeColumns();
        }
        public string ShowFolderSelectionDialog() =>
            installationFolderSelectionDialog.ShowDialog() == DialogResult.OK
                ? installationFolderSelectionDialog.SelectedPath
                : null;
        
        public string ShowPackageScenarioDialog()
        {
            packageScenarioDialog.FileName = $"{scenarioDataGridView.SelectedRows[0].Cells["Name"].Value}.zip";
            return packageScenarioDialog.ShowDialog() == DialogResult.OK
                       ? packageScenarioDialog.FileName
                       : null;
        }

    }
}
