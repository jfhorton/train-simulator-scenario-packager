﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using System;
using System.Collections.Generic;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Views
{
    public interface IMainWindowView
    {
        event EventHandler ApplicationLoaded;

        event EventHandler AutoDetectClicked;

        event EventHandler SetTrainSimulatorFolderClicked;

        event EventHandler SelectedRouteChanged;

        event EventHandler SelectedScenarioChanged;

        event EventHandler PackageScenarioClicked;

        string StatusText { get; set; }
        string TrainSimulatorInstallationFolder { get; set; }

        string ShowFolderSelectionDialog();
        string ShowPackageScenarioDialog();

        string SelectedRoutePath { get; }
        string SelectedScenarioPath { get; }

        string ScenarioZipFileName { get; }

        void ClearRouteList();

        void ClearScenarioList();

        void SetRouteList(List<RouteInfo> routes);

        void SetScenarioList(List<ScenarioInfo> routes);
    }
}