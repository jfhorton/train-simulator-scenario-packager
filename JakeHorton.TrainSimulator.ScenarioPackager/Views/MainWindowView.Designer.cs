﻿
namespace JakeHorton.TrainSimulator.ScenarioPackager.Views
{
    partial class MainWindowView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.routeListBox = new System.Windows.Forms.ListBox();
            this.routesLabel = new System.Windows.Forms.Label();
            this.criteriaTextBox = new System.Windows.Forms.TextBox();
            this.searchLabel = new System.Windows.Forms.Label();
            this.scenariosLabel = new System.Windows.Forms.Label();
            this.packageButton = new System.Windows.Forms.Button();
            this.scenarioDataGridView = new System.Windows.Forms.DataGridView();
            this.trainSimulatorFolderTextbox = new System.Windows.Forms.TextBox();
            this.setTrainSimulatorFolderButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.autoDetectButton = new System.Windows.Forms.Button();
            this.installationFolderSelectionDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.scenariosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.routesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.packageScenarioDialog = new System.Windows.Forms.SaveFileDialog();
            this.mainMenu.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scenarioDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.routesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.helpMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1584, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitMenuItem});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitMenuItem.Text = "E&xit";
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutMenuItem.Text = "&About";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 839);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1584, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(42, 17);
            this.statusLabel.Text = "Ready.";
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 56);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.routeListBox);
            this.splitContainer.Panel1.Controls.Add(this.routesLabel);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.criteriaTextBox);
            this.splitContainer.Panel2.Controls.Add(this.searchLabel);
            this.splitContainer.Panel2.Controls.Add(this.scenariosLabel);
            this.splitContainer.Panel2.Controls.Add(this.packageButton);
            this.splitContainer.Panel2.Controls.Add(this.scenarioDataGridView);
            this.splitContainer.Size = new System.Drawing.Size(1572, 783);
            this.splitContainer.SplitterDistance = 248;
            this.splitContainer.TabIndex = 2;
            // 
            // routeListBox
            // 
            this.routeListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.routeListBox.DisplayMember = "Name";
            this.routeListBox.FormattingEnabled = true;
            this.routeListBox.ItemHeight = 15;
            this.routeListBox.Location = new System.Drawing.Point(3, 18);
            this.routeListBox.Name = "routeListBox";
            this.routeListBox.Size = new System.Drawing.Size(242, 754);
            this.routeListBox.TabIndex = 0;
            this.routeListBox.ValueMember = "Folder Path";
            // 
            // routesLabel
            // 
            this.routesLabel.AutoSize = true;
            this.routesLabel.Location = new System.Drawing.Point(0, 0);
            this.routesLabel.Name = "routesLabel";
            this.routesLabel.Size = new System.Drawing.Size(91, 15);
            this.routesLabel.TabIndex = 1;
            this.routesLabel.Text = "1) Select a route";
            // 
            // criteriaTextBox
            // 
            this.criteriaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.criteriaTextBox.Location = new System.Drawing.Point(54, 757);
            this.criteriaTextBox.Name = "criteriaTextBox";
            this.criteriaTextBox.Size = new System.Drawing.Size(300, 23);
            this.criteriaTextBox.TabIndex = 4;
            // 
            // searchLabel
            // 
            this.searchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.searchLabel.AutoSize = true;
            this.searchLabel.Location = new System.Drawing.Point(3, 761);
            this.searchLabel.Name = "searchLabel";
            this.searchLabel.Size = new System.Drawing.Size(45, 15);
            this.searchLabel.TabIndex = 3;
            this.searchLabel.Text = "Search:";
            // 
            // scenariosLabel
            // 
            this.scenariosLabel.AutoSize = true;
            this.scenariosLabel.Location = new System.Drawing.Point(3, 0);
            this.scenariosLabel.Name = "scenariosLabel";
            this.scenariosLabel.Size = new System.Drawing.Size(171, 15);
            this.scenariosLabel.TabIndex = 2;
            this.scenariosLabel.Text = "2) Select a scenario to package.";
            // 
            // packageButton
            // 
            this.packageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.packageButton.Location = new System.Drawing.Point(1085, 757);
            this.packageButton.Name = "packageButton";
            this.packageButton.Size = new System.Drawing.Size(232, 23);
            this.packageButton.TabIndex = 1;
            this.packageButton.Text = "3) Package scenario...";
            this.packageButton.UseVisualStyleBackColor = true;
            // 
            // scenarioDataGridView
            // 
            this.scenarioDataGridView.AllowUserToAddRows = false;
            this.scenarioDataGridView.AllowUserToDeleteRows = false;
            this.scenarioDataGridView.AllowUserToResizeRows = false;
            this.scenarioDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scenarioDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.scenarioDataGridView.Location = new System.Drawing.Point(3, 18);
            this.scenarioDataGridView.MultiSelect = false;
            this.scenarioDataGridView.Name = "scenarioDataGridView";
            this.scenarioDataGridView.ReadOnly = true;
            this.scenarioDataGridView.RowTemplate.Height = 25;
            this.scenarioDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.scenarioDataGridView.Size = new System.Drawing.Size(1314, 733);
            this.scenarioDataGridView.TabIndex = 0;
            // 
            // trainSimulatorFolderTextbox
            // 
            this.trainSimulatorFolderTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trainSimulatorFolderTextbox.Location = new System.Drawing.Point(134, 27);
            this.trainSimulatorFolderTextbox.Name = "trainSimulatorFolderTextbox";
            this.trainSimulatorFolderTextbox.ReadOnly = true;
            this.trainSimulatorFolderTextbox.Size = new System.Drawing.Size(1184, 23);
            this.trainSimulatorFolderTextbox.TabIndex = 3;
            // 
            // setTrainSimulatorFolderButton
            // 
            this.setTrainSimulatorFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.setTrainSimulatorFolderButton.Location = new System.Drawing.Point(1417, 27);
            this.setTrainSimulatorFolderButton.Name = "setTrainSimulatorFolderButton";
            this.setTrainSimulatorFolderButton.Size = new System.Drawing.Size(164, 23);
            this.setTrainSimulatorFolderButton.TabIndex = 4;
            this.setTrainSimulatorFolderButton.Text = "Set Train Simulator folder...";
            this.setTrainSimulatorFolderButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Train Simulator Folder:";
            // 
            // autoDetectButton
            // 
            this.autoDetectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autoDetectButton.Location = new System.Drawing.Point(1324, 27);
            this.autoDetectButton.Name = "autoDetectButton";
            this.autoDetectButton.Size = new System.Drawing.Size(87, 23);
            this.autoDetectButton.TabIndex = 6;
            this.autoDetectButton.Text = "Auto Detect";
            this.autoDetectButton.UseVisualStyleBackColor = true;
            // 
            // installationFolderSelectionDialog
            // 
            this.installationFolderSelectionDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // packageScenarioDialog
            // 
            this.packageScenarioDialog.DefaultExt = "zip";
            this.packageScenarioDialog.Filter = ".zip archive | *.zip";
            // 
            // MainWindowView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.autoDetectButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.setTrainSimulatorFolderButton);
            this.Controls.Add(this.trainSimulatorFolderTextbox);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainWindowView";
            this.Text = "Train Simulator Scenario Packaging Tool";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scenarioDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.scenariosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.routesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Label routesLabel;
        private System.Windows.Forms.ListBox routeListBox;
        private System.Windows.Forms.Label scenariosLabel;
        private System.Windows.Forms.Button packageButton;
        private System.Windows.Forms.DataGridView scenarioDataGridView;
        private System.Windows.Forms.TextBox trainSimulatorFolderTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button setTrainSimulatorFolderButton;
        private System.Windows.Forms.Button autoDetectButton;
        private System.Windows.Forms.FolderBrowserDialog installationFolderSelectionDialog;
        private System.Windows.Forms.BindingSource scenariosBindingSource;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.BindingSource routesBindingSource;
        private System.Windows.Forms.TextBox criteriaTextBox;
        private System.Windows.Forms.Label searchLabel;
        private System.Windows.Forms.SaveFileDialog packageScenarioDialog;
    }
}

