﻿using Microsoft.Win32;
using static JakeHorton.TrainSimulator.ScenarioPackager.Properties.Settings;

namespace JakeHorton.TrainSimulator.ScenarioPackager
{
    class Utilities
    {
        public static string TrainSimulatorInstallationFolder =>
            (string)Registry.GetValue(
                        keyName: @"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\RailSimulator.com\RailWorks",
                        valueName: "install_path",
                        defaultValue: null);

        public static bool FirstRun
        {
            get => Default.FirstRun;
            set
            {
                Default.FirstRun = value;
                Default.Save();
            }
        }
    }
}
