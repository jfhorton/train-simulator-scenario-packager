﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Models
{
    public class RouteInfo
    {
        public string Name { get; set; }

        public string FolderPath { get; set; }

        private RouteInfo() { }


        public static RouteInfo FromXmlFile(string path)
        {
            var document = new XmlDocument();
            try
            {
                document.Load(path);
                return new()
                {
                    Name = GetRouteName(document),
                    FolderPath = Path.GetDirectoryName(path)
                };
            }
            catch(Exception)
            {
                return null;
            }
        }

        public static RouteInfo FromApArchive(string path) 
        {
            try
            {
                var document = new XmlDocument();
                document.Load(ZipFile.OpenRead(path)
                                     .Entries
                                     .First(file => file.Name == "RouteProperties.xml")
                                     .Open());
                return new()
                {
                    Name = GetRouteName(document),
                    FolderPath = Path.GetDirectoryName(path)
                };
            }
            catch(Exception)
            {
                return null;
            }
        }

        private static string GetRouteName(XmlDocument document) =>
            new List<string>() { "English", "French", "Italian", "German", "Spanish", "Dutch", "Polish", "Russian" }
            .Select(language => document.SelectSingleNode($"/cRouteProperties/DisplayName/Localisation-cUserLocalisedString/{language}").InnerText)
            .First(name => !string.IsNullOrWhiteSpace(name));
    }
}
