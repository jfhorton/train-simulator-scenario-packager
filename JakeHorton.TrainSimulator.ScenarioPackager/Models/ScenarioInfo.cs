﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Models
{
    public class ScenarioInfo
    {
        public string Name { get; set; }
        public string Author { get; set; }

        public string FolderPath { get; set; }
        public string Type { get; set; }

        public DateTime LastModified { get; set; }

        private ScenarioInfo() { }

        public static ScenarioInfo FromXmlFile(string path)
        {
            var document = new XmlDocument();
            try
            {
                document.Load(path);
                return new()
                {
                    Name = GetScenarioName(document),
                    Author = document.SelectSingleNode($"/cScenarioProperties/Author")?.InnerText,
                    FolderPath = Path.GetDirectoryName(path),
                    Type = document.SelectSingleNode($"/cScenarioProperties/ScenarioClass")
                                  ?.InnerText
                                   .Replace("ScenarioClass", string.Empty)[1..],
                LastModified = File.GetLastWriteTime(path)
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string GetScenarioName(XmlDocument document) =>    
            new List<string>() { "English", "French", "Italian", "German", "Spanish", "Dutch", "Polish", "Russian"}
            .Select(language => document.SelectSingleNode($"/cScenarioProperties/DisplayName/Localisation-cUserLocalisedString/{language}").InnerText)
            .First(name => !string.IsNullOrWhiteSpace(name));    
    }
}
