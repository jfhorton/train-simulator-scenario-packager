﻿using System;
using static JakeHorton.TrainSimulator.ScenarioPackager.Properties.Settings;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Models
{
    public class MainWindowModel
    {
        public event EventHandler TrainSimulatorInstallationFolderChanged;

        public static string TrainSimulatorInstallationFolder => Default.TrainSimulatorInstallationFolder;

        public void SetTrainSimulatorInstallationFolder(string folderPath)
        {
            Default.TrainSimulatorInstallationFolder = folderPath;
            Default.Save();
            TrainSimulatorInstallationFolderChanged?.Invoke(this, null);
        }   
    }
}
