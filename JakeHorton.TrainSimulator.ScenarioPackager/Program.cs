using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using JakeHorton.TrainSimulator.ScenarioPackager.Presenters;
using JakeHorton.TrainSimulator.ScenarioPackager.Services;
using JakeHorton.TrainSimulator.ScenarioPackager.Views;
using System;
using System.Windows.Forms;

namespace JakeHorton.TrainSimulator.ScenarioPackager
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var presenter = new MainWindowPresenter(
                mainWindowView: new MainWindowView(),
                messageService: new UIMessageBoxService(),
                mainWindowModel: new MainWindowModel()
            );
            Application.Run(presenter.MainWindowView as Form);
        }
    }
}
