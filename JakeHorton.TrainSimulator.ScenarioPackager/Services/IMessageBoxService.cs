﻿namespace JakeHorton.TrainSimulator.ScenarioPackager.Services
{
    public interface IMessageBoxService
    {
        void ShowErrorDialogOKOnly(string title, string message);
        void ShowInformationDialogOKOnly(string title, string message);
    }
}