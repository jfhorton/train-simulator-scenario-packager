﻿using System.Windows.Forms;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Services
{
    public class UIMessageBoxService : IMessageBoxService
    {
        public void ShowErrorDialogOKOnly(string title, string message) =>
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        

        public void ShowInformationDialogOKOnly(string title, string message) =>
            MessageBox.Show(message, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }
}