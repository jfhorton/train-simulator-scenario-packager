﻿using JakeHorton.TrainSimulator.ScenarioPackager.Models;
using JakeHorton.TrainSimulator.ScenarioPackager.Repositories;
using JakeHorton.TrainSimulator.ScenarioPackager.Services;
using JakeHorton.TrainSimulator.ScenarioPackager.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;

namespace JakeHorton.TrainSimulator.ScenarioPackager.Presenters
{
    public class MainWindowPresenter
    {
        public MainWindowPresenter(
            IMainWindowView mainWindowView,
            IMessageBoxService messageService,
            MainWindowModel mainWindowModel
        )
        {
            MainWindowView = mainWindowView;
            MainWindowModel = mainWindowModel;
            MessageService = messageService;
            WireUpEvents();
        }

        public MainWindowModel MainWindowModel { get; }
        public IMainWindowView MainWindowView { get; }
        public IMessageBoxService MessageService { get; }
        private void AutoDetectTrainSimulatorInstallation()
        {
            var detectTrainSimulatorInstallation = new BackgroundWorker();

            detectTrainSimulatorInstallation.DoWork += (s, e) =>
            {
                MainWindowView.StatusText = "Detecting Train Simulator installation folder...";
                e.Result = Utilities.TrainSimulatorInstallationFolder;
            };

            detectTrainSimulatorInstallation.RunWorkerCompleted += (s, e) =>
            {
                var installationFolder = (string)e.Result;

                if (string.IsNullOrWhiteSpace(installationFolder))
                {
                    MainWindowView.StatusText = "Failed to find your Train Simulator installation.";
                    MessageService.ShowErrorDialogOKOnly(
                        title: "Unable to find your Train Simulator installation. Use the 'Set Train Simulator folder...' button to browse for the installation manually.",
                        message: "Auto detection failed."
                    );
                    return;
                }

                MainWindowModel.SetTrainSimulatorInstallationFolder(installationFolder);
                MainWindowView.TrainSimulatorInstallationFolder = installationFolder;
                MainWindowView.StatusText = "Train Simulator installation found.";
                MessageService.ShowInformationDialogOKOnly(
                    title: "Auto detection succeeded.",
                    message: $"Train Simulator folder was found at:\n{installationFolder}."
                );
                GetRoutes();
            };

            detectTrainSimulatorInstallation.RunWorkerAsync();
            MainWindowView.StatusText = "Ready.";
        }

        private void GetRoutes()
        {
            var loadRouteNames = new BackgroundWorker();

            loadRouteNames.DoWork += (s, e) =>
            {
                MainWindowView.StatusText = "Loading routes. This may take a few seconds...";
                try
                {
                    e.Result = new RouteInformationRepository(MainWindowModel.TrainSimulatorInstallationFolder).GetAll();
                }
                catch (ArgumentException)
                {
                    e.Cancel = true;
                }
            };

            loadRouteNames.RunWorkerCompleted += (s, e) =>
            {
                if (e.Cancelled)
                {
                    MessageService.ShowErrorDialogOKOnly(
                        message: "Content folder was not found. Check that the Train Simulator installation directory is set correctly.",
                        title: "Failed to load routes."
                    );

                    MainWindowView.StatusText = "Failed to load routes.";
                    MainWindowView.ClearRouteList();
                    MainWindowView.ClearScenarioList();
                    
                    return;
                }

                if (e != null)
                {
                    MainWindowView.StatusText = "Routes loaded successfully.";
                    MainWindowView.SetRouteList((List<RouteInfo>)e.Result);
                }
            };
            loadRouteNames.RunWorkerAsync();
        }

        private void GetScenarios()
        {
            if (string.IsNullOrWhiteSpace(MainWindowView.SelectedRoutePath)) return;

            var path = MainWindowView.SelectedRoutePath;
            var loadScenarios = new BackgroundWorker();
            loadScenarios.DoWork += (s, e) =>
            {
                try
                {
                    e.Result = new ScenarioInformationRepository(path).GetAll();
                }
                catch(ArgumentException)
                {
                    e.Result = new List<ScenarioInfo>();
                }
            };
            loadScenarios.RunWorkerCompleted += (s, e) =>
            {
                MainWindowView.SetScenarioList((List<ScenarioInfo>)e.Result);
            };

            loadScenarios.RunWorkerAsync();

        }

        private void LoadApplication()
        {
            if(Utilities.FirstRun)
            {
                MainWindowModel.SetTrainSimulatorInstallationFolder(Utilities.TrainSimulatorInstallationFolder);
                Utilities.FirstRun = false;
            }
            MainWindowView.TrainSimulatorInstallationFolder = MainWindowModel.TrainSimulatorInstallationFolder;
            GetRoutes();
        }

        private void SetTrainSimulatorFolderLocation()
        {
            var result = MainWindowView.ShowFolderSelectionDialog();
            if(result != null)
            {
                MainWindowModel.SetTrainSimulatorInstallationFolder(result);
            }
        }
        

        private void WireUpEvents()
        {
            MainWindowView.ApplicationLoaded += (s, e) => LoadApplication();
            MainWindowView.AutoDetectClicked += (s, e) => AutoDetectTrainSimulatorInstallation();
            MainWindowView.SetTrainSimulatorFolderClicked += (s, e) => SetTrainSimulatorFolderLocation();
            MainWindowView.SelectedRouteChanged += (s, e) => GetScenarios();
            MainWindowView.PackageScenarioClicked += (s, e) => PackageScenario();

            MainWindowModel.TrainSimulatorInstallationFolderChanged += (s, e) =>
            {
                MainWindowView.TrainSimulatorInstallationFolder = MainWindowModel.TrainSimulatorInstallationFolder;
                GetRoutes();
            };
        }

        private void PackageScenario()
        {
            if(string.IsNullOrEmpty(MainWindowView.SelectedScenarioPath))
            {
                MessageService.ShowErrorDialogOKOnly("Package scenario", "Nothing to package.");
                return;
            }

            if (string.IsNullOrEmpty(MainWindowView.ShowPackageScenarioDialog()))
            {
                return;
            }

            var filesToAdd = Directory.GetFiles(MainWindowView.SelectedScenarioPath, "*", SearchOption.AllDirectories);

            using var zipFileStream = new FileStream(MainWindowView.ScenarioZipFileName, FileMode.Create);
            using var archive = new ZipArchive(zipFileStream, ZipArchiveMode.Create);
            foreach (var file in filesToAdd)
            {
                archive.CreateEntryFromFile(file, file.Replace($"{MainWindowModel.TrainSimulatorInstallationFolder}\\", ""));
            }

            MessageService.ShowInformationDialogOKOnly("Package Scenario", "The scenario was successfully packaged.");
        }
    }
}
